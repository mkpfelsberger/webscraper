# Webscraper

Webscraper für Wordpress-Artikel aus DH-Lehre und KiT in PHP.

Beispieldownload ist im Ordner `html` abgelegt.

## Verwendung

(Nur für Artikel in DH-Lehre optimiert!)

1. Alle Dateien downloaden und in der vorliegenden Struktur einem Webserver zugänglich machen

2. Die Hauptdatei `webscraper.php` anpassen: URL des Zielartikels in Zeile 9 bei der Variable `$article_url` eintragen

3. `webscraper.php` vom Webserver ausführen lassen

4. Eine Vorschau wird nach einem ca. 30s dauernden Vorgang angezeit (in der Vorschau werden die Pfade für Bilder nicht richtig aufgelöst)

5. Fertige HTML-Strukur wird unter dem Ordner `html` abgelegt

