<?php

require 'vendor/autoload.php';

// CSS-file von wordpress
$css = GetCSS('https://www.dh-lehre.gwi.uni-muenchen.de/wp-content/themes/graphy/style.css?ver=5.7.2');

// URL des Zielartikels
$article_url = 'https://www.dh-lehre.gwi.uni-muenchen.de/?lehrveranstaltung=ethnolinguistik&v=4';
// $article_url = 'https://www.dh-lehre.gwi.uni-muenchen.de/?lehrveranstaltung=digital-italian-humanities&v=3';


// URL download mit Bereinigung
function GetURL($url, $index) {
  $http_client = new \GuzzleHttp\Client();
  $response = $http_client->get($url);

  $html_string = (string) $response->getBody();

  // HTML is often wonky, this suppresses a lot of warnings
  libxml_use_internal_errors(true);

  $doc = new DOMDocument();
  $doc->loadHTML($html_string);

  $xpath = new DOMXPath($doc);

  $article_html = $xpath->evaluate('//div[@class="content-area"][1]');

  $temp_dom = new DOMDocument();
  foreach($article_html as $n) {
    $temp_dom->appendChild($temp_dom->importNode($n,true));
  }

  $article_html_clean = CleanUp($temp_dom, $index);

  return $article_html_clean;
}

// CleanUp via DOM manipulation
function CleanUp(DOMDocument $temp_dom, $index) {
  // comments
  $comments = $temp_dom->getElementById('comments');
  if (isset($comments)) $comments->parentNode->removeChild($comments);

  // footer
  $all_footer = $temp_dom->getElementsByTagName('footer');
  foreach ($all_footer as $footer) {
    if (isset($footer)) $footer->parentNode->removeChild($footer);
  }

  // Block unter Artikelüberschrift (Veröffentlich am.., von..., Schreib einen Kommentar)
  $finder = new DomXPath($temp_dom);

  $nodes_po = $finder->query("//*[contains(@class, 'posted-on')]");
  foreach ($nodes_po as $node) {
    $node->parentNode->removeChild($node);
  }

  $nodes_bl = $finder->query("//*[contains(@class, 'byline')]");
  foreach ($nodes_bl as $node) {
    $node->parentNode->removeChild($node);
  }

  $nodes_av = $finder->query("//*[contains(@class, 'author vcard')]");
  foreach ($nodes_av as $node) {
    $node->parentNode->removeChild($node);
  }

  $nodes_cl = $finder->query("//*[contains(@class, 'comments-link')]");
  foreach ($nodes_cl as $node) {
    $node->parentNode->removeChild($node);
  }

  $article_html_string = $temp_dom->saveHTML();

  // #################### regex cleanup ##########################
  // Sitzungsnavigation
  $nav_regex = "/<div.*?<div.*?<small>.*?<\/a>.*?<\/small>.*?<\/div>.*?<small>.*?<\/a>.*?<\/small>.*?<\/div>.*?/i";
  $article_html_string = preg_replace($nav_regex, "", $article_html_string);

  // Versionierung
  $version_regex = "/Version: <select.*?<\/select>/";
  $article_html_string = preg_replace($version_regex, "", $article_html_string);

  // Zeige alle in der Veranstaltung
  $zeige_regex = "/<p><small><a href.*?Zeige alle in der Veranstaltung.*?<\/a><\/small><\/p>/";
  $article_html_string = preg_replace($zeige_regex, "", $article_html_string);

  // Nächster/Vorheriger Beitrag
  $beitrag_regex = "/<div><small><a.*<\/a><\/small><\/div>/";
  $article_html_string = preg_replace($beitrag_regex, "", $article_html_string);

  // Veröffentlicht in...
  $ver_regex = '/<div class="entry-meta">?\s*?<span class="">.*?<a.*?<\/a>?\s*?<\/span>?\s*?<\/div>/';
  $article_html_string = preg_replace($ver_regex, "<span></span>", $article_html_string);

  // Zitation der Abschnitte
  $zitation_regex = '/<small><b>Zitation<\/b>/';
  $article_html_string = preg_replace($zitation_regex, "<small><b>Ursprüngliche Zitation des vorliegenden Abschnittes</b>", $article_html_string);

  // font-family: Lora, Georgia, 'Hiragino Mincho ProN'...
  $fontfam_regex = '/style="font-family: Lora, Georgia, .*?, Meiryo, serif; .*?;"/';
  $article_html_string = preg_replace($fontfam_regex, "", $article_html_string);


  // Anchors in Gliederung relativieren
  // if (($offs < 2)) {
  // post ID extrahieren
  // $regex = "/\?p=\d+/";
  // preg_match($regex, $url, $matches);
  // $post_id = trim($matches[0], "?p=");

  if (!$index === true) {

    $anchor_regex = '/(<div class="entry-content"><a href="https\:\/\/www\.dh-lehre\.gwi\.uni-muenchen\.de\/\?p\=)(\d*)(.*?">)/';
    $article_html_string = preg_replace($anchor_regex, '<div class="entry-content"><a href="#post-$2">', $article_html_string);
  }

  // }


  return $article_html_string;
}


// CSS download und direkte Ausgabe
function GetCSS($css_url){
  // CSS href="https://www.dh-lehre.gwi.uni-muenchen.de/wp-content/themes/graphy/style.css?ver=5.7.2"
  $http_client = new \GuzzleHttp\Client();
  $css_response = $http_client->get($css_url);
  $html_css_string = (string) $css_response->getBody();

  // Style für die <p>-Referenzen
  $additional_styles =
  "
  /* <p>-References */
  .p-box {
    color:white !important;
    text-decoration: none !important;
    font-size:0.6em;

    border: 1px solid lightgray;
    background-color: lightgray;
    padding-left:3px;
    padding-right:3px;
    margin-bottom:0px;
    line-height: 1em !important;

    margin-right:0px;

    margin-left:5px;
    float: right;
  }

  .p-box:hover, .p-quote:hover {
    color:black !important;
    background-color: white;
    border-color: black;
  }
  /* Sideline Structure */
  .main-absolute {
    width: 66.5%;
    margin-right: 5px;
    overflow: auto;
    overflow-x: hidden;
    position: absolute;
    left: 0px;
  }
  .side-fixed {
    border: 1px solid #ddd;
    width: 33%;
    position: fixed;
    right: 0px;
    overflow: scroll;
    height: 100%;
  }"
  ;

  return $html_css_string . $additional_styles;
}

// Dateien erstellen
$output_html = "html/index.html";
$output_css = "html/style.css";
unlink($output_html);
unlink($output_css);

// mkdir("html");
// mkdir("html/src");
$output_html = fopen("html/index.html", "w") or die("'html/index.html' konnte nicht angelegt werden");
$output_css = fopen("html/style.css", "w") or die("'html/style.css' konnte nicht angelegt werden");

// CSS download
fwrite($output_css, $css);

$index_page = GetURL($article_url, true);


// links parsen
$dom = new DOMDocument();
$dom->loadHTML($index_page);
$gliederung = $dom->getElementById('gliederung');
$links = $gliederung->getElementsByTagName('a');

// Mitaufende Gliederung
$sideline_structure = $gliederung->ownerDocument->saveHTML($gliederung);

$article_html_string_acc = $index_page;


// durch die links iterieren
foreach ($links as $offs => $link) {

  // Ersten Link überspringen, da der Index sonst doppelt vorhanden ist
  // if ($offs < 1) continue;

  $url = $link->getAttribute('href');

  // html download
  $article_html_string = GetURL($url, false);

  $article_html_string_acc = $article_html_string_acc . $article_html_string;
}


// #################### Picture Management ##########################
function GetIMGs($html_string) {


  // Bilder parsen
  preg_match_all('/<img.*?src=[\'"](.*?)[\'"].*?>/i', $article_html_string_acc, $matches);
  $elements = $matches[1];

  foreach($elements as $element) {

    // mit cURL downloaden:
    // Initialize a file URL to the variable
    $url = $element;

    // User agent, da im GWI-Netz der cURL-Agent nicht akzeptiert wird
    $user_agent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:97.0) Gecko/20100101 Firefox/97.0';

    // Initialize the cURL session
    $ch = curl_init($url);

    // Initialize directory name where
    // file will be save
    $dir = './html/src/';

    // Use basename() function to return
    // the base name of file
    $file_name = basename($url);

    // Save file into file location
    $save_file_loc = $dir . $file_name;

    // Open file
    $fp = fopen($save_file_loc, 'wb');

    // It set an option for a cURL transfer
    curl_setopt($ch, CURLOPT_FILE, $fp);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);

    // Perform a cURL session
    curl_exec($ch);

    // Closes a cURL session and frees all resources
    curl_close($ch);

    // Close file
    fclose($fp);
  }
}
// Pfade der <img>-Elemente anpassen
$img_elements = $dom->getElementsByTagName('img');
foreach ($img_elements as $img_element) {

  $img_regex = '/(src=")(.*?)(\/)(\w+\.\w{3})" /';
  $article_html_string_acc = preg_replace($img_regex, 'src="src/$4"', $article_html_string_acc);
}


// #################### Bibliographie ##########################

$acc_dom = new DOMDocument();
$acc_dom->loadHTML($article_html_string_acc);
$finder = new DomXPath($acc_dom);

$bibs = $finder->query("//*[contains(@class, 'literatur-content')]");

$bibs_array = array();

// Aufbau des arrays:
// (
//     [Malaterra 1928] => Array
//         (
//             [id] => lit:10100
//             [html] => <li id="lit:10100"><b>Malaterra 1928 = </b>Malaterra, Gaufredo (1928): <i>De rebus gestis Rogerii Calabriae et Siciliae comitis et Roberti Guiscardi Ducis fratris eius</i>, in: Ernesto Pontieri, Rerum Italicarum Scriptores 2, V 1, Bologna <a href="http://www.intratext.com/ixt/lat0870/">(Link)</a>.</li>
//         ) ...

foreach ($bibs as $bib) {

  $lis = $bib->getElementsByTagName('li');

  foreach ($lis as $key_id => $li) {
    $li_id = $li->getAttribute('id');

    $li_inner = $li->nodeValue;
    // Das Kürzel soll als key für einen Eintrag dienen
    $li_abr = preg_replace("/ =.*/", "", $li_inner);

    $li_html = $li->ownerDocument->saveHTML($li);

    $bibs_array[$li_abr]['id'] = $li_id;
    $bibs_array[$li_abr]['html'] = $li_html;
  }
  // Einzelne Bibliographien löschen
  $bib->parentNode->removeChild($bib);
}


// array_multisort($bibs_array, SORT_ASC);
sort($bibs_array, SORT_ASC);

// print_r($bibs_array);

$bib_new = '<div class="entry-content"><div class="literatur-content"><h1 id="bibliography"><a href="#bibliography">Bibliographie</a></h1><ul>';

foreach ($bibs_array as $key => $value) {
  $bib_new = $bib_new . $bibs_array[$key]['html'];
}

$bib_new = $bib_new . "</ul></div></div>";


// #################### Sideline Structure ##########################















// ################################################################


// in Datei schreiben
fwrite($output_html, '<div class="main-absolute"> <link rel="stylesheet" href="style.css">' . $article_html_string_acc . $bib_new . '</div><div class="side-fixed">' . $sideline_structure . "</div>");

fclose($output_html);

echo "<style>" . $css . "</style>" . '<div class="main-absolute"> <link rel="stylesheet" href="style.css">' . $article_html_string_acc . '<br>' . $bib_new . '<br></div><div class="side-fixed">' . $sideline_structure . "</div>";
?>
